const router = require('express').Router()
const bodyParser = require('body-parser')
const restrictApi = require('../middlewares/restrict-api')

const auth = require('../controllers/api/authController')

router.use(bodyParser.json())

router.post('/v1/auth/login', auth.login)
router.get('/v1/auth/whoami', restrictApi, auth.whoami)

module.exports = router;